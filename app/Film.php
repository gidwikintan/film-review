<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Genre;

class Film extends Model
{
    protected $table = "films";
    protected $fillable = ["judul", "ringkasan", "tahun", "poster", "genre_id"];
    public $timestamps = false;
    public function genre()
    {
        return $this->belongsTo('App\Genre', 'genre_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
    public function perans()
    {
        return $this->hasMany('App\Peran');
    }
}
