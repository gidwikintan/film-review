<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "komentars";
    protected $fillable = ["isi", "rating", "user_id", "film_id"];


    public function film()
    {
        return $this->belongsTo('App\Film', 'film_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
