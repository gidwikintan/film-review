<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $fillable = ["nama", "film_id", "cast_id"];
    public $timestamps = false;
    public function film()
    {
        return $this->belongsTo('App\Film', 'film_id');
    }
    public function cast()
    {
        return $this->belongsTo('App\Cast', 'cast_id');
    }
}
