<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Film;
use App\Review;
use App\User;
use App\Peran;
use RealRashid\SweetAlert\Facades\Alert;


class MovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::with(['genre',])->take(5)->get();
        return view('pages.movie', compact('film'));
    }

    public function show($id)
    {
        $film = film::find($id);
        $komentar = Review::where('film_id', $id)->get();;
        $peran = Peran::where('film_id', $id)->get();
        // dd($komentar);
        return view('pages.detail', compact('film', 'komentar', 'peran'));
    }

    public function store(Request $request)
    {

        $review = Review::create([
            "isi" => $request["komentar"],
            "rating" => $request["rating"],
            "user_id" => Auth::id(),
            "film_id" => $request["film_id"],
        ]);
        $id =  $request["id"];
        Alert::success('Success Title', 'Success Message');
        return redirect('/film');
    }
}
