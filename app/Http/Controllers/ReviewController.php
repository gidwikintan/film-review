<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Film;
use App\Review;
use App\User;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $komentar = Review::find($id);
        // dd($komentar);
        return view('pages.review', compact('komentar'));
    }
}
