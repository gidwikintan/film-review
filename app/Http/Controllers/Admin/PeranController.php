<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Peran;
use App\Film;
use App\Cast;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perans = Peran::all();
        return view('admin.peran.index', compact('perans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $film = Film::all();
        $cast = Cast::all();
        return view('admin.peran.create', compact(['film', 'cast']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|unique:perans',
            'film_id' => 'required',
            'cast_id' => 'required',
        ]);

        Peran::create([
            'nama' => $request['nama'],
            'film_id' => $request['film_id'],
            'cast_id' => $request['cast_id'],
        ]);

        return redirect('peran')->with('success', 'Post Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $perans = Peran::find($id);
        return view('peran.show', compact('perans'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peran = Peran::find($id);
        $film = Film::all($id);
        $cast = Cast::all($id);
        return view('admin.peran.edit', compact(['peran', 'film', 'cast']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $peran = Peran::where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['film_id'],
            'bio' => $request['cast_id'],
        ]);
        return redirect('peran', compact('peran'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Peran::destroy($id);
        return redirect('peran');
    }
}
