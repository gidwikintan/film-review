<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Film;
use App\Genre;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $film = Film::all();
        return view('admin.home', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('admin.input-movie', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|unique:films',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:jpeg,jpg,png',
            'genre_id' => 'required',
        ]);


        $gambar = $request->poster;
        $name_img = time() . '-' . $gambar->getClientOriginalName();

        Film::create([
            "judul" => $request->judul,
            "ringkasan" => $request->ringkasan,
            "tahun" => $request->tahun,
            "poster" => $name_img,
            "genre_id" => $request->genre_id,
        ]);

        $gambar->move('images', $name_img);

        return redirect('/movie')->with('success', 'Post Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = film::find($id);
        return view('pages.komentar', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = Genre::all();
        $film = Film::findorfail($id);
        return view('admin.edit', compact('film', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:jpeg,jpg,png',
            'genre_id' => 'required',
        ]);

        $film = Film::findorfail($id);

        if ($request->has('poster')) {
            $path = "images/";
            File::delete($path . $film->poster);
            $gambar = $request->poster;
            $name_img = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('images', $name_img);

            $file_data = ([
                "judul" => $request->judul,
                "ringkasan" => $request->ringkasan,
                "tahun" => $request->tahun,
                "poster" => $name_img,
                "genre_id" => $request->genre_id,
            ]);
        } else {
            $file_data = ([
                "judul" => $request->judul,
                "ringkasan" => $request->ringkasan,
                "tahun" => $request->tahun,
                "genre_id" => $request->genre_id,
            ]);
        }
        $film->update($file_data);
        return redirect('/movie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::findorfail($id);
        $film->delete();

        $path = "images/";
        File::delete($path . $film->poster);

        return redirect('/movie');
    }
}
