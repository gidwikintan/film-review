<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Genre;
use Illuminate\Http\Request;
use App\Film;
use App\User;
use App\Review;

use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $film = Film::with(['genre'])->take(4)->get();
        return view('pages.home', compact('film'));
    }
    public function show($id)
    {
        $film = Film::find($id);
        $user = User::all();
        return view('pages.komentar', compact('film', 'user'));
    }
    public function store(Request $request)
    {
        Review::create([
            'isi' => $request->komentar,
            'rating' => $request->rating,
            'user_id' => Auth::id(),
            'film_id' => $request->film_id,
        ]);

        return redirect('/')->with('success', 'Post Berhasil Disimpan');
    }
}
