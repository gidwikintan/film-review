<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = "genres";
    protected $fillable = ["nama"];


    public function users()
    {
        return $this->hasMany('App\User');
    }
    public function films()
    {
        return $this->hasMany('App\Film');
    }
}
