<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $fillable = ["nama", "umur"];
    public $timestamps = false;
    public function perans()
    {
        return $this->hasMany('App\Peran');
    }
}
