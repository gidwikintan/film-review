<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::resource('/movie', 'Admin\MovieController');
Route::resource('/cast', 'Admin\CastController');
Route::resource('/peran', 'Admin\PeranController');
Route::resource('/film', 'MovieController');
Route::resource('/review', 'ReviewController');
Auth::routes();

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
});
