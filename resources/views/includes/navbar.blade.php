<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">moviz</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/film">Movies</a>
        </li>
        <li class="nav-item">
        <a class="nav-link btn btn-secondary text-white" href="#" tabindex="-1" aria-disabled="true">login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link btn btn-secondary text-white ml-3" href="/movie" tabindex="-1" aria-disabled="true">dashboard</a>
            </li>
    </ul>
    </div>
    </div>
</nav>