@extends('adminlte.master')

@section('content')
<main class="text-dark">
    <div class="main ">
        <div class="container">       
        <a href="{{route('movie.create')}}" class="btn btn-primary m-2" style="background-color: white; color: black">Tambah</a>    
        </div>
    </div>
    <div class="list mt-3">
        <div class="container">
            <h5>List Movies</h5>
            <div class="row d-flex justify-content-">
                @foreach ($film as $key=>$value)
                <div class="col-3">
                    <div class="card" >
                        <img src="images/{{$value->poster}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$value->judul}}</h5> <br>
                            <h6>{{$value->genre->nama}}</h6>
                            <p class="card-text">{{$value->ringkasan}}</p>
                            <a href="/movie/{{$value->id}}/edit" class="btn btn-secondary">Edit</a>
                            <form action="/movie/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-secondary mt-1" value="Delete">
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</main>
@endsection
