@extends('adminlte.master')

@section('title')
    Add new Cast
@endsection

@section('content')

<div class="movie-detail m-5">
        <h4>Add new Cast</h4>
        <form role="form p-5" method="POST" action="/cast">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama"  value="{{old('nama', '')}}"" placeholder="Masukkan Nama">
                @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
            </div>
            <div class="form-group">
                <label for="Umur">Umur</label>
                <input type="number" class="form-control" id="umur" placeholder="Umur" name="umur" value="{{old('umur', '')}} ">
                @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
            
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            
        </form>
</div>

@endsection