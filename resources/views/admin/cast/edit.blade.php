@extends('adminlte.master')

@section('content')
<div class="movie-detail m-5">
    <h4>Edit Cast</h4>

    <form role="form p-5" method="POST" action="/cast">
        @csrf
        
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama"  value="{{old('nama', '')}}"" placeholder="Masukkan Nama">
            @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        
    </form>
</div>
@endsection