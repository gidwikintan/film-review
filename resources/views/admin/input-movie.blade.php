@extends('layouts.app')

@section('title')
    Upload Your Movie
@endsection

@section('content')

<div class="movie-detail mt-3">
    <div class="container">
        <h4>Upload Your Movie</h4>
        <form action="/movie" method="POST" enctype="multipart/form-data">
            @csrf
            
            <div class="form-group">
                <label for="judul">Movie Title</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Enter Your Movies Title">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea name="ringkasan" id="ringkasan" class="form-control" rows="5" placeholder="Enter ..."></textarea>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Year</label>
                <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Enter Your Movie">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Movies Poster</label>
                <div class="input-group">
                    <div class="custom-file">
                    <input type="file" name="poster" id="poster">
                    
                    @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleSelectBorder">Movies Genre</label>
                
                <select class="custom-select form-control-border" name="genre_id" id="genre_id" >
                    @foreach ($genre as $key=>$value)
                    <option value="{{$value->id}}">{{$value->nama}}</option>
                    @endforeach
                </select>
                @error('genre_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Upload</button>
        </form>
    </div>
</div>

@endsection
@push('scripts')
    <script>
  var editor_config = {
    path_absolute : "/",
    selector: 'textarea.my-editor',
    relative_urls: false,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table directionality",
      "emoticons template paste textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    file_picker_callback : function(callback, value, meta) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
      if (meta.filetype == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.openUrl({
        url : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no",
        onMessage: (api, message) => {
          callback(message.content);
        }
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endpush