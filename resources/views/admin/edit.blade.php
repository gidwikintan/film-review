@extends('layouts.app')

@section('title')
    Upload Your Movie
@endsection

@section('content')

<div class="movie-detail mt-3">
    <div class="container">
        <h4>Upload Your Movie</h4>
        <form action="/movie/{{$film->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">Movie Title</label>
                <input type="text" class="form-control" name="judul" id="judul" value=" {{$film->judul}}">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea name="ringkasan" id="ringkasan" class="form-control" rows="5" placeholder="{{$film->ringkasan}}" value="{{$film->ringkasan}}"></textarea>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Year</label>
                <input type="text" class="form-control" name="tahun" id="tahun" value="{{$film->tahun}}">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Movies Poster</label>
                <div class="input-group">
                    <div class="custom-file">
                    <input type="file" name="poster" id="poster">
                    
                    @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleSelectBorder">Movies Genre</label>
                
                <select class="custom-select form-control-border" name="genre_id" id="genre_id" >
                    @foreach ($genre as $key=>$value)
                    @if ($value->id == $film->genre_id)
                    <option value="{{$value->id}}" selected>{{$value->nama}}</option>
                    @else
                    <option value="{{$value->id}}">{{$value->nama}}</option>
                    @endif
                    @endforeach
                </select>
                @error('genre_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>            
            <button type="submit" class="btn btn-primary">Upload</button>
        </form>
    </div>
</div>

@endsection