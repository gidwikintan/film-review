@extends('adminlte.master')

@section('title')
    Add New Peran
@endsection

@section('content')

<div class="movie-detail m-5">
        <h4>Add New Peran</h4>
        <form role="form p-5" method="POST" action="/peran">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama"  value="{{old('nama', '')}}"" placeholder="Masukkan Nama">
                @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
            </div>
            <div class="form-group">
                <label for="exampleSelectBorder">Film</label>
                
                <select class="custom-select form-control-border" name="film_id" id="film_id" >
                    @foreach ($film as $key=>$value)
                    <option value="{{$value->id}}">{{$value->judul}}</option>
                    @endforeach
                </select>
                @error('film_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleSelectBorder">Cast</label>
                
                <select class="custom-select form-control-border" name="cast_id" id="cast_id" >
                    @foreach ($cast as $key=>$value)
                    <option value="{{$value->id}}">{{$value->nama}}</option>
                    @endforeach
                </select>
                @error('cast_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            
        </form>
</div>

@endsection