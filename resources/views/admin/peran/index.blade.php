

@extends('adminlte.master')

@section('content')
    <div class="card">
        <div class="card-header with-border">
        <h3 class="card-title">Bordered Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <a class="btn btn-secondary" href="/peran/create">Add new perans</a>
        <table class="table table-bordered">
            <thead><tr>
            <th style="width: 10px">#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th style=>Bio</th>
            <th style="width:40px">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($perans as $key=>$peran)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$peran->nama}}</td>
                    <td>{{$peran->film_id}}</td>
                    <td>{{$peran->cast_id}}</td>
                    <td>
                        <a href="/peran/{{$peran->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/peran/{{$peran->id}}" method="post">
                            @csrf
                            @method('delete')
                        <button class="btn btn-danger mt-1" type="submit">Hapus</button>
                        </form>
                    </td>
                </tr>
            @empty
            <tr>
                
                <td colspan="5" align="center">None</td>
            </tr>
            @endforelse
        </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        {{-- <div class="card-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
            <li><a href="#">«</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">»</a></li>
        </ul>
        </div> --}}
    </div>
@endsection