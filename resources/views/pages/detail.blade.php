@extends('layouts.app')
@section('title')
    Movic- movie review
@endsection
@push('script-head')
    <script src="/path-to-your-tinymce/tinymce.min.js"></script>
@endpush
@section('content')
<main class="text-white">
    <div class="main ">
        <div class="container">
            <div class="row mt-5">
                <div class="col-6 d-flex justify-content-center align-items-center">
                    <img src="/images/{{$film->poster}}" class="d-block w-100 my-5" style="height: 700px;" alt="...">
                </div>
                <div class="col-6 mt-5 ">
                    
                    <h1>{{$film->judul}}</h1>
                    <br>
                    <p class="badge badge-secondary">{{$film->genre->nama}}</p>
                    <h3>Description</h3>
                    <p>{{$film->ringkasan}}</p>
                    <br>
                    <h3>Cast</h3>
                    @foreach ($peran as $key=>$value)
                    <p>{{$value->cast->nama}} as {{$value->nama}}</p>
                    @endforeach
                    <div>
                        <form action="/film" method="POST" class="mt-5">
                            @csrf
                                <div class="form-group sm-form-group">
                                    <label>Review</label>
                                    <!-- <textarea name="komentar" id="komentar" class="form-control" rows="5" placeholder="Berikan Review Anda"></textarea> -->
                                    <textarea name="komentar" class="form-control my-editor" placeholder="Berikan Review Anda"></textarea>
                                </div>
                                <div class="form-group sm-form-group">
                                    <label>Ratting</label>
                                    <select class="custom-select form-control-border" name="rating" id="rating" >
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option> 
                                    </select>
                                </div>
                                <div class="form-group sm-form-group">
                                    <input type="hidden" class="form-control" name="film_id" id="film_id" value="{{$film->id}}">
                                </div>
                                <button type="submit" class="btn btn-primary">Post Review</button>
                        </form>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h1>Review</h1> <br>
                </div>
                @foreach ($komentar as $key=>$value)
                    <div class="col-3">
                        <div class="card m-2" style="height: 180px;color: black">
                            <div class="card-body">
                                <h4 class="card-title">{{$value->user->name}}</h4>
                                <p class="card-text" style="font-size: 15px;">{{$value->isi}}</p>
                                <p>Rating {{$value->rating}}/10</p>
                                <a href="/review/{{$value->id}}" class="btn btn-sm btn-secondary">Selengkapnya</a>   
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            
        </div>
    </div>
</main>
@endsection
