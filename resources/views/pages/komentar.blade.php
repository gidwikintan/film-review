@extends('layouts.app')

@section('content')
<main class="text-white">
    <div class="main ">
        <div class="container">
            <div class="col">
                <div class="card" >
                    <img src="/images/{{$film->poster}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title" style="color: black">{{$film->judul}}</h5>
                        <p class="card-text" style="color: black">{{$film->ringkasan}}</p>
                        <form action="/film" method="POST" class="mt-5">
                            @csrf
                            <div class="form-group sm-form-group">
                                <label for="judul" >komentar</label>
                                <textarea name="komentar" id="komentar" class="form-control" rows="3" placeholder="Berikan Review"></textarea>
                            </div>
                            <div class="form-group sm-form-group">
                                <label for="judul">Ratting</label>
                                <input type="text" class="form-control" name="rating" id="rating" placeholder="Masukan Ratting 1-10">
                            </div>
                            <div class="form-group sm-form-group">
                                <input type="text" class="form-control" name="film_id" id="film_id" value="{{$film->id}}">
                            </div>
                            <button type="submit" class="btn btn-primary">Post Komentar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
