@extends('layouts.app')
@section('title')
    Movic- movie review
@endsection

@section('content')
<main class="text-white">
    <div class="main ">
        <div class="container">
            <div class="row mt-3">
                <div class="col-5 d-flex justify-content-center align-items-center">
                    <h1>Get to know your favorite movies!</h1>
                </div>
                <div class="col-7">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="/images/wallpaperflare.com_wallpaper (1).jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/wallpaperflare.com_wallpaper (2).jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/wallpaperflare.com_wallpaper.jpg" class="d-block w-100" alt="...">
                            </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                            </a>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="list mt-5">
        <div class="container">
            <h2>List Movies</h2>
            <div class="row d-flex justify-content-">
                @foreach ($film as $key=>$value)
                <div class="col-3">
                    <div class="card" style="height: 530px;">
                        <img src="images/{{$value->poster}}" style="width: 100%; height: 300px" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$value->judul}}</h5>
                            <p class="badge badge-secondary">{{$value->genre->nama}}</p>
                            <p class="card-text" style="font-size: 12px;">{{$value->ringkasan}}</p>
                            <a href="/film/{{$value->id}}" class="btn btn-sm btn-secondary">Selengkapnya</a>   
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    </div>
</main>
@endsection
