@extends('layouts.app')

@section('title')
    Movie
@endsection

@section('content')
 <div class="container">
    <div class="row d-flex justify-content-">
        @foreach ($film as $key=>$value)
        <div class="col-3">
            <div class="card" style="height: 530px;color: black">
                <img src="images/{{$value->poster}}" style="width: 100%; height: 300px" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{$value->judul}}</h5>
                    <p class="badge badge-secondary">{{$value->genre->nama}}</p>
                    <p class="card-text" style="font-size: 12px;">{{$value->ringkasan}}</p>
                    <a href="/film/{{$value->id}}" class="btn btn-sm btn-secondary">Selengkapnya</a>   
                </div>
            </div>
        </div>
        @endforeach
    </div>
<div class="container">
@endsection
