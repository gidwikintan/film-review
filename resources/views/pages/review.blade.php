@extends('layouts.app')
@section('title')
    Movic- movie review
@endsection

@section('content')
<main class="text-white">
    <div class="main ">
        <div class="container">
            <div class="blog-post">
        <h2 class="blog-post-title">{{$komentar->film->judul}}</h2>
        <p class="blog-post-meta">bye {{$komentar->user->name}}</a></p>

        
        <blockquote>
          <p>{{$komentar->isi}}</p>
        </blockquote>
        
      </div>
        </div>
    </div>
</main>
@endsection